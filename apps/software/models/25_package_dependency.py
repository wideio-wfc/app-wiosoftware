# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- encoding:utf-8 -*-
import zipfile
import datetime
import os
import urllib
import json
import sys

from django.core.urlresolvers import reverse
from django.contrib.auth.models import AbstractUser  # , User
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render_to_response, RequestContext
from django.contrib.contenttypes import generic
from django.contrib.contenttypes.models import ContentType
import backoffice.lib as debug
from wioframework.amodels import *
from extfields.fields import *
from wioframework import decorators as dec
#from wioframework.models import File
from references.models import Image, LinkReference, BibliographicReference
import wioframework.fields as models

from accounts.models import UserAccount


PACKAGE_DEP_ANYVERSION = 0
PACKAGE_DEP_GTVERSION = 1
PACKAGE_DEP_LTVERSION = 2
PACKAGE_DEP_EQVERSION = 3


@wideiomodel
class PackageDependency(models.Model):

    """
    Describes specific requirement of package
    """
    ctrname_p="package dependencies"
    from_package = models.ForeignKey('Package', related_name="pd_dependencies")    
    to_package = models.ForeignKey('Package', related_name="pd_enabling")
    refversion = JSONField()
    relation = models.IntegerField(
        choices=[
            (-2, '<'), (-1, "<=",), (0, '=='), (1, '>='), (2, '>')])

    class WIDEIO_Meta:
        permissions = dec.perm_for_admin_only
