# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- encoding:utf-8 -*-
import zipfile
import datetime
import os
import urllib
import json
import sys

from django.core.urlresolvers import reverse
from django.contrib.auth.models import AbstractUser  # , User
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render_to_response, RequestContext
from django.contrib.contenttypes import generic
from django.contrib.contenttypes.models import ContentType
import backoffice.lib as debug
from wioframework.amodels import *
from extfields.fields import *
from wioframework import decorators as dec
#from wioframework.models import File
from references.models import Image, LinkReference, BibliographicReference
import wioframework.fields as models

from accounts.models import UserAccount
from django.core import validators


@wideio_publishable()
@wideio_owned("author")
@wideio_timestamped
@wideiomodel
class PackageBranch(models.Model):
    """
    This describes a packagebranch for a model/or an asset. 
    
    IE a set of code / software and its evolution
    (whether they occur as consequence of the build process or as a consequence of new releases)
    
    Each packagebranch (/software) may provide numerous elements to platform, such as algorithms and datasets.
    
    Initial package content can be provided a zip file that contains all the files required to run a scientific software.
    Each package must contain metadata files that allows the system to compute dependencies and so on...
    """
    ctrname_p="package branches"
    
    name = models.CharField(
        max_length=64,
        db_index=True,
        app_validators=[
            validators.RegexValidator(
                settings.NAME_RE)])
    description = models.TextField(db_index=True)
    license = models.ForeignKey('software.License', null=True, blank=True)
    is_public=models.BooleanField(default=True,help_text="Use private packagebranch for code that you'd like to keep private")

    def on_add(self,request):
        if request.REQUEST["_AUTO_ADD_PACKAGE"]:
            from software.models import Package
            p=Package()
            p.branch=self
            p.on_add(request)
            p.target_architecture = "amd64"
            p.target_base_distribution = request.REQUEST.get("_BASE_DISTRIBUTION_ID",None)
            p.container_url = None

            #default_execution_environment =
            #compatible_execution_environment = # compute.environment 
            p.source_package = None
            p.status = 1
            p.packagebranch = self

            # =====================================================================================
            p.version = [1,0,0,0]
            p.giturl = None
            p.zipfile = None
    
            p.validation_job = None
            p.installation_job = None
            p.compilation_job = None
            #p.dependencies #< PUT THE RUNNING EN
            
            p.save()
            if request.REQUEST["_ATTACH_PACKAGE_TO_WORKER"]:
                from cloud.models import Worker
                w=Worker.objects.get(id=request.REQUEST["_ATTACH_PACKAGE_TO_WORKER"])
                if w.used_by_id!=request.user.id:
                        raise Exception("PermissionError %s != %s"%( w.used_by_id,request.user.id))
                w.node.send_reqrep(request.user,{'wid':w.wid,'opcode':"send_msg#set_container_mode",'args':{'container_id':p.id}})

                
    def can_delete(self, request):
        return self.packages.count()==0
                
    def on_delete(self,request):
        ##
        ## TODO ENSURE THAT THE PACKAGE IS NOT ATTACHED ANYMOE
        ## 
        for p in list(self.packages.all()):
            p.delete()
                
    def get_all_references(self):
        return [license]
    
    def get_latest_package(self):
        pl=self.packages.all()
        p= pl[0]        
        return p
    
    def get_all_algorithms(self):
        return self.get_latest_package().get_all_algorithms()

    def __unicode__(self):
        return self.name

    class WIDEIO_Meta:
        NO_DRAFT=True
        search_enabled = ["name", "description"]
        icon = 'ion-ionic'
        permissions = dec.perm_for_logged_users_only

        class Actions:
            @wideio_action(possible=lambda x,r:((x.author==r.user) or (r.user.is_staff)) and (not x.is_public))
            def make_public(self,request):
                self.is_public=True
                self.save()          
                return 'alert("ok");'                
            @wideio_action(possible=lambda x,r:((x.author==r.user) or (r.user.is_staff)) and (x.is_public))
            def make_private(self,request):
                self.is_public=False
                self.save()
                return 'alert("ok");'
            @wideio_action(possible=lambda x,r:((x.author==r.user) or (r.user.is_staff)) and "schedreqrep_id" in r.GET,icon="icon-edit")
            def edit_latest_version(self, request):
                pl=self.packages.all()
                p = pl[0]
                from compute.models import SchedReqRep
                from cloud.models import Worker
                s=SchedReqRep.objects.get(id=request.GET["schedreqrep_id"])
                w=s.get_worker()
                w.node.send_reqrep(request.user,{'wid':w.wid,'opcode':"send_msg#set_container_mode",'args':{'container_id':p.id}})
                return 'alert("ok'+str(w.wid)+'");'
            @wideio_action(possible=lambda x,r:((x.author==r.user) or (r.user.is_staff)) and "schedreqrep_id" in r.GET,icon="ion-power")
            def load_latest_package(self, request):
                pl=self.packages.all()
                p= pl[0]
                from compute.models import SchedReqRep
                from cloud.models import Worker
                s=SchedReqRep.objects.get(id=request.GET["schedreqrep_id"])
                w=s.get_worker()
                w.node.send_reqrep(request.user,{'wid':w.wid,'opcode':"send_msg#add_dependencies",'args':{'dependencies':[d.id for d in p.get_all_dependencies()]}})
                return 'alert("ok'+str(w.wid)+'");'            
