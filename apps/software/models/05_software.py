#!/usr/bin/env python
# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- encoding:utf-8 -*-

import wioframework.fields as models
from wioframework.amodels import wideiomodel, wideio_owned, get_dependent_models, wideio_timestamped, wideio_action
from wioframework import decorators as dec


# class LimitsSpec(models.Model):
# data=models.IntegerField()
# memory=models.IntegerField()
# cputime=models.IntegerField()
# processes=models.IntegerField()


### ######################################################################
# MISC STUFF
### ######################################################################

@wideiomodel
class VersionID(models.Model):
    major = models.IntegerField(default=1)
    minor = models.IntegerField(default=0)
    micro = models.IntegerField(default=0)
    build = models.IntegerField(default=0)
    tag = models.CharField(blank=True, null=True, max_length=128)

    def __unicode__(self):
        return "(%d.%d.%d(%d-%s))" % (self.major,
                                      self.minor,
                                      self.micro,
                                      self.build,
                                      self.tag)

    class WIDEIO_Meta:
        permissions = dec.perm_read_logged_users_write_for_staff_only


@wideiomodel
class Software(models.Model):

    """
    Used to describe one of the WIDE IO tools
    """
    title = models.CharField(max_length=128)
    internal_description = models.TextField()
    public_description = models.TextField()

    def __unicode__(self):
        return self.title

    class WIDEIO_Meta:
        permissions = dec.perm_read_logged_users_write_for_staff_only


@wideiomodel
class GitRepoVersion(models.Model):
    branchlocation = models.TextField()
    revno = models.IntegerField()

    class WIDEIO_Meta:
        permissions = dec.perm_read_logged_users_write_for_staff_only


@wideiomodel
class SoftwareVersion(models.Model):

    """
    Used to describe one version of our software
    """
    software = models.ForeignKey(Software)
    #versionid = EmbeddedModelField(VersionID, verbose_name="ID")
    versiontitle = models.CharField(max_length=256, verbose_name="Title")
    # changelog= #< embedded via git / need for cache ?
    #gitids = ListField(EmbeddedModelField(GitRepoVersion))  # git reposversion

    class WIDEIO_Meta:
        permissions = dec.perm_read_logged_users_write_for_staff_only
