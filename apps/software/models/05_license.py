# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- encoding:utf-8 -*-
import zipfile
import datetime
import os
import urllib
import json
import sys

from django.core.urlresolvers import reverse
from django.contrib.auth.models import AbstractUser  # , User
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render_to_response, RequestContext
from django.contrib.contenttypes import generic
from django.contrib.contenttypes.models import ContentType
import backoffice.lib as debug
from wioframework.amodels import *
from extfields.fields import *
from wioframework import decorators as dec
import wioframework.fields as models


@wideio_moderated()
@wideio_owned()
@wideio_timestamped
@wideiomodel
class License(models.Model):
    """
    License

    This is the text of a license that the package may use to distribute the assets.
    """
    name = models.CharField(max_length=256)
    shortdescription = models.TextField(
        verbose_name="Short description",
        blank=True,
        max_length=1000,
        help_text="Informal description allowing to quickly understand the usual key points of the legal text.")
    fulltext = models.TextField(
        max_length=50000,
        verbose_name="Full text",
        help_text="Blob containing the full text of the license")   
    is_opensource = models.BooleanField(
        default=False,
        help_text="Is it an opensource license ?")
    is_free_software = models.BooleanField(
        default=False,
        help_text="Is it a free software ?")
    
    is_public = models.BooleanField(
        default=False,
        help_text="Is the text of the license public ?")


    def __unicode__(self):
        return self.name

    class WIDEIO_Meta:
        permissions = dec.perm_read_logged_users_write_for_admin_only


@wideiomodel
@wideio_owned("user")
@wideio_timestamped
class LicenseAcceptance(models.Model):

    """
    This ties a license acceptance to a specific package.

    A LICENSE acceptance binds three elements : a user, a package, and a license
    """

    related_asset = models.ForeignKey('Package')
    license = models.ForeignKey(License)
    date_accepted = models.DateTimeField()
    #ip_accepted = models.IPAddressField()

    def __unicode__(self):
        return "License accepted : %r by %r for %r" % (self.license, self.user, self.related_asset)

    class WIDEIO_Meta:
        permissions = dec.perm_read_logged_users_write_for_admin_only
