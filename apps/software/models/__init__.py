#!/usr/bin/env python
# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- encoding:utf-8 -*-

"""
This modules contains all the elements related to the deployment of software stacks
on the cloud.

It includes WIDE IO package system.
"""

import os
import sys
#from django.core.urlresolvers import reverse
# from django.contrib.auth.models import AbstractUser # , User
#from django.views.decorators.csrf import csrf_exempt
#from django.shortcuts import render_to_response, RequestContext
#from django.contrib.contenttypes import generic
#from django.contrib.contenttypes.models import ContentType
#import aiosciweb1.backoffice.lib as debug
#from wioframework.amodels import *
#from wioframework.fields import *
#from wioframework import decorators as dec
#from wioframework.models import File
#from references.models import Image, LinkReference, BibliographicReference
import wioframework.fields as models

MODELS = []


# ep0=__path__[0]
ep0 = os.path.dirname(os.path.dirname(__file__))
ep1 = os.path.basename(ep0)


for d in os.listdir(os.path.join(ep0, "models")):
    if (d[0] != "_") and d.endswith(".py"):
        d = d[:-3]
        md = ep1 + ".models." + d
        m = __import__("%s.models.%s" % (ep1, d), fromlist=[ep1, "models"])
        __module = locals()
        for f in dir(m):
            if f not in __module:
                v = __module[f] = getattr(m, f)
                # print f
                # if hasattr(v,"__module__"):
                # print v,v.__module__==md,v.__module__,md ,
                # issubclass(v,models.Model)
                try:
                    if hasattr(
                        v,
                        "__module__") and v.__module__ in [
                        md,
                        ep1] and issubclass(
                        v,
                            models.Model):
                        MODELS.append(v)
                except:
                    pass


for m in MODELS:
    m.app_name = ep1
    if hasattr(m._meta, "model_name"):
        m._meta.model_name = m.__name__  # "models"
    else:
        m._meta.module_name = m.__name__
    # print "AL1",m._meta.app_label
    # sys.stdin.readline()
    m._meta.app_label = ep1
    m._meta.db_table = ("%s_%s" % (m.app_name, m.__name__)).lower()
    m.__module__ = ep1


def on_load_postprocess(cache):
    import sys
    for m in MODELS:
        if (hasattr(m, "on_load_postprocess")):
            m.on_load_postprocess()
    try:
        if callable(cache.app_labels[ep1]):
            cache.app_labels[ep1] = []
        if not m in cache.app_labels[ep1]:
            cache.app_labels[ep1].append(m)
        cache.register_models(ep1, *MODELS)
    except Exception as e:
        print e
