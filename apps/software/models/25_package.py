# ############################################################################
# |W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|W|I|D|E|I|O|L|T|D|
#                          ..-.:.:...
#                       :.-- -     ..:...
#                   :.:. -             -.:...
#               :.:. -                     ..:...
#           :.:. .            _;:__.          ...-...
#       :.:. .               :;    -+_    -|       .--...
#   :.-- .                    -=      -~-.-           -.:...
# -:...              ___.      -=_                        -.--
# ...    .          =;  --=_     :=                   ..   ...
# .-.      . .              ~-___=;               . -      .:.
# ...           -.                             -.          ...
# .:.                .                    . .              .:.
# ...                   -.             -.                  ...
# .:.                      ...    . -                -~4>  .:.
# ...       _^+_.              -.                       2  ...
# .:.           ~,              .                 /'   _(  .:.
# ....           <              -          +'  ^LJ>   _^   ...
# .:..          _);             -     _   J   _/  ~~-'    .:.
# ....        _&i^i             .   _~_, <(   .^           ...
#  :.       _v>^  <             .  _X~'  -s,               .:.
#  :.             -=            .   S      ^'              ...
#  :....           -=_  ,       .   2                    ..-.:
#     :....          -^^        .                    .-.:. .
#        ..:...                 .                 ..:. -
#            -.:...             .           . :.-- -
#                 :....         .         -.-- .
#                    -.:...     .    ..:.: -
#                         -.:......--. -
#                             -.:. .
# 
# Copyright (c) 2012-2015 All Right Reserved, WIDE IO LTD,
# http://wide.io/
# ----------------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License.
#     
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#     
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see http://www.gnu.org/licenses/
# This work is released under GPL v 3.0
# ----------------------------------------------------------------------
# For all information : copyrights@wide.io
# ----------------------------------------------------------------------
# 
# |D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|D|O|N|O|T|R|E|M|O|V|E|!|
# ############################################################################
# -*- encoding:utf-8 -*-
from wioframework.amodels import *
from extfields.fields import *
from wioframework import decorators as dec
import wioframework.fields as models

from accounts.models import UserAccount

def only_once(l):
    m=set()
    rl=[]
    for e in l:
        if e not in m:
            m.add(e)
            rl.append(e)
    return rl


@wideiomodel
class Package(models.Model):
    """
    This corresponds to a specific version of a file / package
    """

    target_architecture = models.CharField(max_length=32)
    target_base_distribution = models.ForeignKey(
        'compute.BaseDistribution',
        blank=True,
        null=True,
        related_name="available_packages")
    container_url = models.CharField(max_length=512, null=True, blank=True)

    default_execution_environment = models.ForeignKey(
        'compute.Environment',
        blank=True,
        null=True,
        related_name="preferred_for_packages")  # provide a way to know target architecture if binary
    compatible_execution_environment = models.ManyToManyField(
        'compute.Environment',
        null=True,
        blank=True,
        related_name="compatible_with_packages")
    source_package = models.ForeignKey(
        'Package',
        null=True,
        blank=True,
        related_name="compiled_packages")

    status = models.IntegerField(default=0, choices=(
        (0, 'NOT YET UPLOADED'),
        (1, 'UPLOAD CONFIRMED'),
        (2, 'COMPILED'),
        (3, 'API VERIFIED'),
    ))

    packagebranch = models.ForeignKey(
        'PackageBranch',
        null=True,
        blank=True,
        related_name="packages")

    # =====================================================================================
    version = JSONField()

    giturl = models.URLField(blank=True, null=True)
    zipfile = models.ForeignKey(
        'extfields.File',
        verbose_name="ZIP file",
        blank=True,
        null=True)

    validation_job = models.ForeignKey(
        'compute.Job',
        null=True,
        blank=True,
        related_name='validated_packages')
    installation_job = models.ForeignKey(
        'compute.Job',
        null=True,
        blank=True,
        related_name='installed_packages')
    compilation_job = models.ForeignKey(
        'compute.Job',
        null=True,
        blank=True,
        related_name='compiled_packages')

    ##
    ## Requirements / dependencies
    ##

    #@property
    #def dependencies(self):
    #    return self.dp_dependencies
    
    #@property
    #def enabling(self):
    #    return self.dp_enabling    
    
    dependencies = models.ManyToManyField(
        'software.Package',
        through='software.PackageDependency',
        related_name="enabling")

    def do_install(self, request, **kwargs):
        from compute.lib import create_privileged_job
        result = create_privileged_job(
            request,
            "wideio_install_package",
            self.id,
            **kwargs)
        print "Install finished with result:" + str(result)
        return result

    def do_compile(self, request, **kwargs):
        """
        This calls shall take a target arhcitecture as it is possible to compile for many different clouds/architecture
        """
        from compute.lib import create_job
        result = create_job(
            request,
            "wideio_compile_package",
            self.id,
            **kwargs)
        print "Compile finished with result: " + str(result)
        return result

    def do_validate_package(self, request, **kwargs):
        """
        Validate is not only validation, validation shall also registesr all the necessay
           - algorithms
           - superglue converters
           - datasets

        that are part of the algorithm.
        """
        from compute.lib import create_job
        result = create_job(request, "wio_package_validate", self.id, **kwargs)
        print "Validate finished with result: " + str(result)
        return result

    def on_add(self, request):
        pass

    def can_update(self,request):
        return self.packagebranch.can_update(request)
    
    def can_delete(self,request):
        return self.packagebranch.can_update(request) and (self.enabling.count()==0)    
    
    def __unicode__(self):
        return unicode(self.packagebranch) + " " + unicode(self.version)

    @property
    def package(self):
        print "(use of package instead of packagebranch) deprecated !!"
        return self.packagebranch
    
    
    def get_all_dependencies(self):
        return only_once(reduce(lambda x,y:x+y.get_all_dependencies(),self.dependencies.all(),[]))+[self]

    class WIDEIO_Meta:
        icon = 'ion-social-dropbox-outline'
        search_enabled = ['packagebranch__name']
        mandatory_fields = ['packagebranch', 'version', 'giturl', 'zipfile']
        form_exclude = [
            'status',
            'validation_job',
            'installation_job',
            'compilation_job',
            'source_package']
        permissions = dec.perm_for_logged_users_only

        class Actions:

            @wideio_action(possible=lambda o, r: r.user.is_superuser)
            def compile(self, request):
                self.do_compile(request)

            @wideio_action(possible=lambda o, r: r.user.is_superuser)
            def install(self, request):
                self.do_install(request)
                
            @wideio_action(possible=lambda o, r: True,mimetype="text/html") #< FIXME
            def open_terminal_to_use_package(self, request):
                r=map(lambda x:x.id,self.get_all_dependencies())
                from cloud.models import Node
                req=Node.get_available_compute_node().start_ssh_server(request.user,dependencies=r)
                return {'_redirect':req.get_view_url()}
            
            @wideio_action(possible=lambda o, r: True,mimetype="text/html") #< FIXME
            def open_terminal_to_edit_package(self, request):
                r=map(lambda x:x.id,self.get_all_dependencies())
                from cloud.models import Node
                assert(r[-1]==self.id)
                r=r[:-1]
                req=Node.get_available_compute_node().start_ssh_server(request.user,dependencies=r, request_update={"mount_mode":"container","container_id":self.id})
                return {'_redirect':req.get_view_url()}
